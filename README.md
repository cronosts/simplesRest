# Instruções de uso

pip install flask

python app.py

# Métodos
O app irá servir no localhost na porta 8080

## GET /
Retornara a lista de itens em memória

## POST /
Adicionará um elemento

## PUT /indice
Substitui o elemento do indice especificado na url pelo enviado

## DELETE /indice
Remove o elemento no indice especificado


# Exemplos

## Exemplo de json

``` json
{
    "nome": "Fulano 1"
    , "idade": 15
}

```

## Exemplo retorno do GET

``` json
[
    {
        "nome": "Fulano 1"
        , "idade": 15
    }
    , {
        "nome": "Fulano 1"
        , "idade": 15
    }
]

```
