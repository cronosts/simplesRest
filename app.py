from flask import Flask, jsonify, request, make_response

app = Flask('SimpleRest')

db = []

@app.route('/', methods=['GET'])
def get():
    return jsonify(db)

@app.route('/', methods=['POST'])
def post():
    obj = request.json
    try:
        db.append({
            'nome': str(obj['nome'])
            , 'idade': int(obj['idade'])
        })
        return jsonify({ "indice": len(db) - 1 })
    except Exception as e:
        return 'Erro: "{}"'.format(e), 500

@app.route('/<int:indice>', methods=['PUT'])
def put(indice):
    obj = request.json
    try:
        db[indice] = {
            'nome': str(obj['nome'])
            , 'idade': int(obj['idade'])
        }
        return jsonify({ "indice": len(db) - 1 })
    except Exception as e:
        return 'Erro: "{}"'.format(e), 500

@app.route('/<int:indice>', methods=['DELETE'])
def delete(indice):
    try:
        obj = db[indice]
        db.remove(obj)
        return jsonify(obj)
    except Exception as e:
        return 'Erro: "{}"'.format(e), 500

if __name__ == '__main__':
    app.run('localhost', 8080)